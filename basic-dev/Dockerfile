FROM ubuntu:20.04

# Avoid warnings by switching to noninteractive
ENV DEBIAN_FRONTEND=noninteractive

ARG USERNAME=basicdev
ARG HOME=/home/$USERNAME
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN groupadd --gid $USER_GID $USERNAME \
    && useradd -s /bin/bash --uid $USER_UID -g $USERNAME --create-home $USERNAME

# Disable setrlimit(RLIMIT_CORE) message
RUN echo "Set disable_coredump false" >> /etc/sudo.conf

RUN apt-get -y update \
    && apt-get -y install --no-install-recommends apt-utils 2>&1 \
    && apt-get -y install  git 

ENV PATH="$HOME/.local/bin:$PATH"
# Symlink python3 and pip3 to python and pip
RUN ln -s /usr/bin/python3 /usr/bin/python & \
    ln -s /usr/bin/pip3 /usr/bin/pip

RUN mkdir /workspaces && chown $USERNAME:$USERNAME workspaces
WORKDIR /workspaces

# Grab the files from gitlab
ADD https://gitlab.com/api/v4/projects/13741845/repository/files/.devcontainer%2fubuntu.20.04.sh/raw?ref=master /workspaces/ubuntu.20.04.sh

ADD https://gitlab.com/api/v4/projects/13741845/repository/files/.devcontainer%2frequirements.txt/raw?ref=master /workspaces/requirements.txt

RUN chown $USERNAME:$USERNAME /workspaces/requirements.txt

# Run the install script
RUN chmod +x ubuntu.20.04.sh && ./ubuntu.20.04.sh

USER $USERNAME

# Install Python dependencies from requirements.txt if it exists
RUN if [ -f "requirements.txt" ]; then pip install --user -r requirements.txt; fi

# Switch back to dialog for any ad-hoc use of apt-get
ENV DEBIAN_FRONTEND=dialog

# Set the default shell to bash rather than sh
ENV SHELL /bin/bash
